<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Mastercv;
use App\Models\Mastercveducation;
use App\Models\Mastercvexperience;
use App\Models\Mastercvskill;

class MastercvController extends BaseController
{
	public function index()
	{
		$result = new Mastercv();

		$data = [
			'title_meta' => view('partials/title-meta', ['title' => 'CV']),
			'page_title' => view('partials/page-title', ['title' => 'Curriculum Vitae', 'li_1' => 'Curriculum Vitae', 'li_2' => 'List Curriculum Vitae']),
			'records' => $result->findAll()
		];

		return view('cv/index', $data);
	}

	public function add()
	{

		$result = new Mastercv();

		$data = [
			'title_meta' => view('partials/title-meta', ['title' => 'CV']),
			'page_title' => view('partials/page-title', ['title' => 'Add', 'li_1' => 'List Curriculum Vitae', 'li_2' => 'Add Curriculum Vitae']),
			'records' => $result->findAll()
		];

		return view('cv/add', $data);
	}

	public function save()
	{
		$cv = new Mastercv();
        $cvSkill = new Mastercvskill();
        $cvExperience = new Mastercvexperience();
        $cvEd = new Mastercveducation();

        $profile = [
            'name' => $this->request->getVar('name'),
            'title' => $this->request->getVar('title'),
            'description' => $this->request->getVar('about'),
            'email' => $this->request->getVar('email-address'),
            'phone' => $this->request->getVar('phone-number'),
        ];
        $cv->insert($profile);
        $cv_id = $cv->getInsertID();

        for ($i=0; $i < count($this->request->getVar('skills')); $i++) { 
            $skills = [
                'master_cv_id' => $cv_id,
                'name' => $this->request->getVar('skills')[$i]
            ];
            $cvSkill->insert($skills);
        }

        for ($i=0; $i < count($this->request->getVar('company')); $i++) { 
            $data = [
                'master_cv_id' => $cv_id,
                'title' => $this->request->getVar('job-title')[$i],
                'company_name' => $this->request->getVar('company')[$i],
                'description' => $this->request->getVar('job-description')[$i],
                'start_date' => $this->request->getVar('job-start-date')[$i],
                'end_date' => $this->request->getVar('job-end-date')[$i]
            ];
            $cvExperience->insert($data);
        }

        for ($i=0; $i < count($this->request->getVar('univ')); $i++) { 
            $data = [
                'master_cv_id' => $cv_id,
                'title' => $this->request->getVar('univ-major')[$i],
                'name' => $this->request->getVar('univ')[$i],
                'start_date' => $this->request->getVar('univ-start-date')[$i],
                'end_date' => $this->request->getVar('univ-end-date')[$i]
            ];
            $cvEd->insert($data);
        }

        var_dump($skills);
	}
}
