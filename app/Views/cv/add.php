<?= $this->extend('layout/main-layout') ?>

<?= $this->section('content') ?>

<div class="page-content" id="app">
    <div class="container-fluid">

        <!-- start page title -->
        <?= $page_title ?>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- header  -->
                        <div class="d-flex flex-wrap align-items-center mb-4">
                            <h4 class="me-2"><b>Add Curriculum Vitae</b></h4>
                        </div>
                        
                        <form enctype='multipart/form-data' action="<?= base_url(route_to('cv-save')) ?>" method="post">
                            <div class="accordion" id="accordionPanelsStayOpenExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                                            Profile
                                        </button>
                                    </h2>
                                    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                                        <div class="accordion-body">
                                            <div class="mb-3">
                                                <label class="form-label">Name</label>
                                                <input name="name" required class="form-control" type="text">
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Title</label>
                                                <input name="title" required class="form-control" type="text">
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Phone Number</label>
                                                <input name="phone-number" required class="form-control" type="number">
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Email Address</label>
                                                <input name="email-address" required class="form-control" type="email">
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">About</label>
                                                <textarea class="form-control" name="about" cols="30" rows="6"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="skills-headingTwo">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#skills-collapseOne" aria-expanded="true" aria-controls="skills-collapseOne">
                                            Skills
                                        </button>
                                    </h2>
                                    <div id="skills-collapseOne" class="accordion-collapse collapse" aria-labelledby="skills-headingTwo">
                                        <div class="accordion-body">
                                            <button type="button" class="btn btn-success me-3" v-on:click="add('skills')">
                                                <i class='bx bx-plus' ></i> Add
                                            </button>
                                            <button type="button" class="btn btn-danger" v-on:click="remove('skills')">
                                                <i class='bx bx-minus' ></i> Remove
                                            </button>
                                            <div class="mt-3" v-for="i in skills">
                                                <input 
                                                    name="skills[]"
                                                
                                                    class="form-control"
                                                    type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="experiences-headingThree">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#experiences-collapseOne" aria-expanded="true" aria-controls="experiences-collapseOne">
                                            Experiences
                                        </button>
                                    </h2>
                                    <div id="experiences-collapseOne" class="accordion-collapse collapse" aria-labelledby="experiences-headingThree">
                                        <div class="accordion-body">
                                            <button type="button" class="btn btn-success me-3" v-on:click="add('experiences')">
                                                <i class='bx bx-plus' ></i> Add
                                            </button>
                                            <button type="button" class="btn btn-danger" v-on:click="remove('experiences')">
                                                <i class='bx bx-minus' ></i> Remove
                                            </button>
                                            <div v-for="i in experiences">
                                                <hr>
                                                <div class="mb-3">
                                                    <label class="form-label">Company</label>
                                                    <input name="company[]" class="form-control" type="text">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Title</label>
                                                    <input name="job-title[]" class="form-control" type="text">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Start Date</label>
                                                    <input name="job-start-date[]" class="form-control" type="date">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">End Date</label>
                                                    <input name="job-end-date[]" class="form-control" type="date">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Description</label>
                                                    <textarea class="form-control" name="job-description[]" cols="30" rows="6"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="educations-headingFour">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#educations-collapseOne" aria-expanded="true" aria-controls="educations-collapseOne">
                                            Educations
                                        </button>
                                    </h2>
                                    <div id="educations-collapseOne" class="accordion-collapse collapse" aria-labelledby="educations-headingFour">
                                        <div class="accordion-body">
                                            <button type="button" class="btn btn-success me-3" v-on:click="add('educations')">
                                                <i class='bx bx-plus' ></i> Add
                                            </button>
                                            <button type="button" class="btn btn-danger" v-on:click="remove('educations')">
                                                <i class='bx bx-minus' ></i> Remove
                                            </button>
                                            <div v-for="i in educations">
                                                <hr>
                                                <div class="mb-3">
                                                    <label class="form-label">University</label>
                                                    <input name="univ[]" class="form-control" type="text">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Majoring</label>
                                                    <input name="univ-major[]" class="form-control" type="text">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Start Date</label>
                                                    <input name="univ-start-date[]" class="form-control" type="date">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">End Date</label>
                                                    <input name="univ-end-date[]" class="form-control" type="date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-success mt-5" type="submit">Save</button>
                        </form>

                    </div>
                </div>
                <!-- end card -->
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div> <!-- container-fluid -->
</div>
<script src="https://unpkg.com/vue@3"></script>
<script>
        const { createApp } = Vue

        createApp({
            data() {
                return {
                    skills: 1,
                    experiences: 1,
                    educations: 1
                }
            },
            methods: {
                add(type) {
                    switch (type) {
                        case 'skills':
                            this.skills += 1
                            break;
                        case 'experiences':
                            this.experiences += 1
                            break;
                        case 'educations':
                            this.educations += 1
                            break;
                        default:
                            break;
                    }
                },
                remove(type) {
                    switch (type) {
                        case 'skills':
                            this.skills -= 1
                            break;
                        case 'experiences':
                            this.experiences -= 1
                            break;
                        case 'educations':
                            this.educations -= 1
                            break;
                        default:
                            break;
                    }
                }
            }
        }).mount('#app')
    </script>

<?= $this->endSection() ?>